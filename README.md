# Abastecimento de Insumos em Linhas de Aplicação

## Participantes

### Início do escopo do projeto:

- Renan Zulian
- Johan Maheashi
- Fernando Silva Maransatto
- Christopher Martinez Tsen
- Felipe Barboza

------------

## Etapas

- [x] Identificar a necessidade e criar um escopo inicial
- [ ] Identificar como criar plugin no QGIS (Teste)
- [ ] Definir escopo do plugin (o que é automático, e o que é manual?)
- [ ] Criar um mapa com dados reais
- [ ] Desenvolvimento do Plugin
- [ ] Testes e Validação com especialista da área
- [ ] Publicação na comunidade do QGIS

------

## Dados de Entrada

- Malha Vetorial; (shape file)
- Linhas de Aplicação; (shape file)
- Tamanho da Barra de Aplicação;
- Capacidade de Armazenamento da Máquina;

------

## Malha Vetorial

Tipo vetor, com tabela de atributos, composta de um arquivo shape.
Tem tamanho fixo (ex: 10m)

![](entenda-uma-malha-vetorial.png)

![](exemplo-de-malha-vetorial.png)

## Linha de Aplicação

![](entenda-uma-linha-de-aplicacao.png)

------

## Sequência da Solução

![](sequencia-da-solucao.png)

### 1. Gerar malha vetorial

![](lembrete-1-ha-10mil-metros.png)

Importando malha vetorial
(definir caminho no programa)
![](importando-malha-vetorial.jpeg)

### 2. Definir linhas de aplicação

Conceito
![](definir-linhas-de-aplicacao.png)

Gerando Shape File das linhas no QGIS
(definir caminho no programa)
![](gerando-shape-file.jpeg)

Gerado as linhas dentro do shape file
(definir caminho no programa)
![](gerando-as-linhas-dentro-do-shapefile.jpeg)


### 3. Aplicação do buffer das linhas

Conceito
![](fase-1-buffer.png)

Define os polígonos do buffer das linhas (para usar no plugin "Estatísticas por Zona")
![](poligono-dos-buffers-da-linha.jpeg)

### 4. Recortando o buffer com base na malha vetorial

Conceito
![](recortando-buffer.png)

Recortando o buffer com base na malha vetorial
![](recortando-buffer-com-base-na-malha-vetorial.jpeg)

Conhecendo a área real da aplicação
![](conhecendo-a-area-real-da-aplicacao.jpeg)

<font color="red"><i>
Encontramos problema com a versão do QGIS na hora de transformar os polígonos em raster

...no qgis madeira 3.4 não tem o plugin Estatística por Zona (os prints próximos serão incluídos posteriormente na versão 2.17)
</i>
</font>


### 5. Início do cálculo

![](inicio-do-processo-concluido.png)

Percorrer as linhas para aplicar o cálculo
![](realizar-o-calculo-percorrendo-as-linhas.png)

Exemplo do cálculo
![](exemplo-de-calcull.png)

Linhas pintadas
![](linhas-pintadas-amarelas.png)

Pontos definidos
![](pontos-de-abastecimento-definidos.png)


---

## Dificuldades Técnidas
![](dificuldades-tecnicas.png)